#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 abonengo (j.astronomicon@gmail.com)

import os, sys
import json
import requests
from PIL import Image, ImageDraw, ImageFont

prog_name = "astronotation.py"

#*****************************************
# Settings. Change them as you wish
#*****************************************
color = "white"
# color: supports Hexadecimal colors (like "#ff0000" if you want it red)
# or common HTML color names like "red" or "Red"
font = "Ubuntu-M.ttf"
# font: a filename or file-like object containing a TrueType font. Ex:
# font = "Arial.ttf"
# If the file is not found in this filename, the loader may also search in
# other directories, such as the fonts/ directory on Windows
# or /Library/Fonts/, # /System/Library/Fonts/ and ~/Library/Fonts/ on macOS
# or /usr/share/fonts/ on most Linux distributions.
# The font variable may be updated depending on your font choice
# and your OS. The example here works for Ubuntu Linux :
# /usr/share/fonts/truetype/freefont/ubuntu/Ubuntu-M.ttf
linewidth = 2
# linewidth: line width in pixel
min_radius = 15
# min_radius: in pixel. Sets minimum radius for the circles
fontsize = 28
#*****************************************

# Get command line arguments
if len(sys.argv) == 3:
    # Get image file
    img_file = sys.argv[1]
    try:
        im = Image.open(img_file)
    except IOError:
        print("Error: File '%s' does not exist or is not an image" % img_file)
        raise SystemExit('Aborting...')
    # Get job_id
    job_id = sys.argv[2]
    if not job_id.isdigit():
        print("Error: %s not a number" % job_id)
        raise SystemExit('Aborting...')
else:
    raise SystemExit(print("Error: " + prog_name + " takes 2 arguments.\n" \
    "Usage: python3 " + prog_name + " <filename.jpg> <job_id>\n" \
    "Aborting..."))

# Get source image information
print("Reading image "+ img_file)
img_filename, img_ext = os.path.splitext(img_file)
img_width, img_height = im.size
print("Image : ", img_width, "x", img_height, "pixels")

# Read json from API result
print("Querying nova.astrometry.net...")
url = "http://nova.astrometry.net/api/jobs/"+str(job_id)+"/annotations/"
response = requests.get(url)
try:
    data = response.json()
except:
    print("Error: %s is not a valid job ID" % job_id)
    raise SystemExit('Aborting...')

# Uncomment the lines if you want to read JSON from file
# filename = "objects.json"
# if filename:
#     with open(filename, "r") as f:
#         data = json.load(f)

# Create image
img = Image.new("RGBA", (img_width, img_height), (0,0,0,0))

# Get objects list
objects = data["annotations"]

for obj in objects :
    name = obj["names"][0]
    x = obj["pixelx"]
    y = obj["pixely"]
    r = obj["radius"]

    # Adding a minimal radius size
    if r < min_radius:
        r = min_radius

    # Label line top y position
    line_y = y-r-r*0.5

    # Sets minimum length for the label lines
    if (y-r)-line_y < 15:
        line_y = y-r-15

    # Label settings
    fnt = ImageFont.truetype(font, fontsize, encoding="unic")
    text_x = x-(len(name)*fontsize/4)
    text_y = line_y-fontsize*1.2

    # If the label is outside image borders
    if (x+(len(name)*fontsize/2)) > img_width :
        text_x = x-(len(name)*fontsize/2)
    if (x+(len(name)*fontsize/2)) < 0 :
        text_x = x+(len(name)*fontsize/2)
    if text_y < fontsize + 6 :
        text_y = text_y + fontsize
        line_y = line_y + fontsize

    print("Processing object " + name)

    # Drawing annotations
    draw = ImageDraw.Draw(img)
    draw.ellipse((x-r, y-r, x+r, y+r), outline =(color), width=linewidth)
    draw.line((x, y-r, x, line_y), fill=(color), width=linewidth)
    draw.text((text_x,text_y), name, font=fnt, fill=(color))

print("Saving annotations image as '"+img_filename+"-annotations.png'")
img.save(img_filename+"-annotations.png")
print("Done...")
