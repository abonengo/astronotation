# astronotation

Astronotation is a Python 3 script that draws annotations of the identified sky objects on an astrophotography. It uses on the results of the calibration done with [astrometry.net](http://nova.astrometry.net) and adds labels to the stars and the galaxies.

Of course, it is already possible to obtain the same results with the annotated images on astrometry.net, but you can't change the colors or the parameters of the annotations and you always have to keep this hard green color. Not the best if you want to reuse them !

Fortunately, Astronotation is a script that allows you to adapt and change the rendering of the annotations on your astrophotography images.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

First of all, you need an astrophotography image (preferably JPG) and you should have upload it at [astrometry.net](http://nova.astrometry.net). As said in astrometry.net homepage, you just have to "*input an image and we'll give you back astrometric calibration meta-data, plus lists of known objects falling inside the field of view.*"

Here is an astrophoto :

![original image](https://gitlab.com/abonengo/astronotation/-/raw/master/image.jpg)

At the end of the process, which may take some time, you'll end with a job process ID : write it down.

### Usage

Before running it, be sure to have astronomicon.py and the image you want to annotate in the same directory.

The program takes two arguments :
* the filename of your image
* the astrometry.net job process ID

So, in a terminal :
```python
python3 astronotation.py <filename> <job_ID>
```

For example :
```python
python3 astronotation.py image.jpg 4704444
```

The script generates a PNG image containing labels and circles around stars and galaxies positions.

Open then an image editor and put your original image on a layer and the annotations image (.png) on another (a top) layer.

Export as another image and you're done !

Here is an example of a result :

![annotated image](https://gitlab.com/abonengo/astronotation/-/raw/master/image-annotated.jpg)

### Parameters

You can edit the python script and change the parameters in order to draw your own and specific annotations :

* color: Supports Hexadecimal colors (like "#ff0000" if you want it red) or common HTML color names like "red" or "Red". See also pillow library documentation at https://pillow.readthedocs.io/en/stable/reference/ImageColor.html
* font: A filename or file-like object containing a TrueType font. If the file is not found in this filename, the loader may also search in or /Library/Fonts/, /System/Library/Fonts/ and ~/Library/Fonts/ on macOS or /usr/share/fonts/ on most Linux distributions. The font variable may be updated depending on your font choice  and your OS. The example here works for Ubuntu Linux : /usr/share/fonts/truetype/freefont/ubuntu/Ubuntu-M.ttf. See also https://pillow.readthedocs.io/en/stable/reference/ImageFont.html
* linewidth: Change here line width in pixel
* min_radius : Sets minimum radius for the circles around the stars, in pixel.
* fontsize: change fontsize here. Default is 28 pt.

### Examples

You can see more examples of annotated astrophotos on my website :

https://www.astronomicon-notebook.com/index.php/2020/04/11/a-python-script-for-astrophotography-annotations/

Feel free to leave helpful comments!


## Authors

* **Abonengo** - *Initial work* - [Abonengo](https://gitlab.com/abonengo)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* [astrometry.net](http://nova.astrometry.net) and its API for all plate solving computations
* [astrobin](https://www.astrobin.com) for the inspiration
